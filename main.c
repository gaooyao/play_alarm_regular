#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#pragma comment(lib, "Winmm.lib")

typedef struct s_time
{
    int hour;
    int minute;
    char m_name[64];
    struct s_time *next;
} S_TIME;

int play(char *m_name)
{

    PlaySound(m_name, 0, SND_FILENAME); //1.wav是要播放的音乐文件
    return 0;
}

int main()
{
    S_TIME time_list = {.next = NULL};
    S_TIME *time_list_point = &time_list;
    FILE *time_table = fopen("time_table.txt", "r");
    while (!feof(time_table))
    {
        S_TIME *temp = (S_TIME *)malloc(sizeof(S_TIME));
        temp->next = NULL;
        fscanf(time_table, "%d %d %s", &temp->hour, &temp->minute, temp->m_name); //读取一行
        time_list_point->next = temp;
        time_list_point = time_list_point->next;
    }
    fclose(time_table);
    while (1)
    {
        time_list_point = time_list.next;
        SYSTEMTIME sys;
        GetLocalTime(&sys);
        while (time_list_point)
        {
            if (time_list_point->hour == sys.wHour)
            {
                if ((sys.wMinute - time_list_point->minute >= 0) && (sys.wMinute - time_list_point->minute <= 5))
                {
                    play(time_list_point->m_name);
                    Sleep(300000);
                    break;
                }
            }
            time_list_point = time_list_point->next;
        }
        Sleep(60000);
    }
}
